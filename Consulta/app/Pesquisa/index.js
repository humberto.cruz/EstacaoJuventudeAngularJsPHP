estacaoApp.cC({
	name: 'pesquisaController',
	inject: [
		'$scope',
		'$location',
		'ModelService',
		'DataService',
		'GMapsService',
		'uiGmapGoogleMapApi',
		'Geolocation',
	],
	data: {
		GMS: 'GMapsService'
	},
	init: function() {
		// Indicação da "página" atual para o menu
		this.DataService.actualPath = this.$location.path();
		// Esperando o carragamento da API GMaps
		this.uiGmapGoogleMapApi.then(function(maps) {
			this.Geolocation.getAddress().then(function(address){
				this.GMS.userGeo = address;
				this.GMS.map.markers.usuario = [];
				this.GMS.map.markers.usuario.push(
					{
						idKey: 'usuario_1',
						coords: {
							latitude: address.coordenadas[0],
							longitude: address.coordenadas[1]
						},
						zoom: 15,
						options: {
							labelContent: 'Você',
							labelClass:'label label-primary',
							labelAnchor: '14 -1'
						},
						icon: 'img/markers/usuario.png'
					}
				);
				this._searchGeoMunicipio();
			}.bind(this));
		}.bind(this));
	},
	watch: {
		'{object}DataService.appAndUserReady()':'_startApp',
		'{object}GMS.search.estado_id':'getMunicipios',
		'{object}GMS.visao':'_changeVisao'
	},
	methods: {
		_startApp: function(newv, oldv) {
			// Iniciando App
			if (newv != true) return false;
			
			this.getTematicas();
			this.getEstados();
			//this.getOcorrencias();
			
			this.afterStart();
		},
		afterStart: function() {
			// Após Iniciar
			
			//this.listTematicas();
			
		},
		// Verifica se há ação/programa ativo no municipio Geolocalizado
		_searchGeoMunicipio: function() {
			this.ModelService.Api.getRecordsByFilter(
				{
					table_name: 'municipios_ocorrencias',
					filter: 'lower(sem_acento(nome)) like lower(sem_acento(\'' + this.GMS.userGeo.cidade + '\'))',
				},
				function(success) {
					if (success.record.length > 0) {
						this.GMS.search.estado_id = success.record[0].estado_id;
						this.GMS.search.municipio_id = success.record[0].id;
						this.GMS.geoActions.setVisao('A');
					} else {
						this._searchGeoEstado();
					}
				}.bind(this),
				function(error) {
				}
			);
		},
				// Verifica se há ação/programa ativo no estado Geolocalizado
		_searchGeoEstado: function() {
			this.ModelService.Api.getRecordsByFilter(
				{
					table_name: 'estados_ocorrencias',
					filter: 'lower(sem_acento(nome)) like lower(sem_acento(\'' + this.GMS.userGeo.estado + '\'))',
				},
				function(success) {
					if (success.record.length > 0) {
						this.GMS.search.estado_id = success.record[0].id;
						this.GMS.geoActions.setVisao('M');
					} else {
						this.GMS.geoActions.setVisao('E');
					}
				}.bind(this),
				function(error) {
				}
			);
		},
		// Clique nos marcadores
		clickMarkerUsuario: function(marker, eventName, objetc) {
			this.GMS.map.center.latitude = this.GMS.map.markers.usuario[0].coords.latitude;
			this.GMS.map.center.longitude = this.GMS.map.markers.usuario[0].coords.longitude;
			this.GMS.map.zoom = this.GMS.map.markers.usuario[0].zoom;
		},
		clickMarkerEstado: function(marker, eventName, object) {
			this.GMS.data.estados.forEach(function(estado){
				if (estado.id == object.estado_id) {
					this.GMS.map.center.latitude = object.coords.latitude;
					this.GMS.map.center.longitude = object.coords.longitude;
					this.GMS.map.zoom = object.zoom;
					this.GMS.search.estado_id = estado.id;
				}
			}.bind(this));
		},
		clickMarkerMunicipio: function(marker, eventName, object) {
			this.GMS.data.municipios.forEach(function(municipio){
				if (municipio.id == object.municipio_id) {
					this.GMS.map.center.latitude = object.coords.latitude;
					this.GMS.map.center.longitude = object.coords.longitude;
					this.GMS.map.zoom = object.zoom;
					this.GMS.search.estado_id = municipio.id;
				}
			}.bind(this));
		},
		clickMarkerLocalizacao: function(marker, eventName, object) {
			console.log(marker);
			console.log(eventName);
			console.log(object);
		},
		_changeVisao: function(newv, oldv) {
			if (newv == oldv) return false;
			
			if (newv == 'E') {
				this.GMS.map.center = this.GMS.brasil.center;
				this.GMS.map.zoom = this.GMS.brasil.zoom;
				this.getEstados();
				this.getOcorrencias();
			}
			
			if (newv == 'M') {
				this.getMunicipios(null);
				this.getOcorrencias();
			}
			
			if (newv == 'A') {
				this.getOcorrencias();
			}
			
			if (newv == 'U') {
				this.GMS.map.center.latitude = this.GMS.map.markers.usuario[0].coords.latitude;
				this.GMS.map.center.longitude = this.GMS.map.markers.usuario[0].coords.longitude;
				this.GMS.map.zoom = this.GMS.map.markers.usuario[0].zoom;
			}
		},
		getTematicas: function() {
			this.ModelService.Tematica.table_name = this.ModelService.tables.Tematica;
			this.ModelService.Tematica.order = 'nome ASC';
			this.ModelService.Api.getRecordsByFilter(
				this.ModelService.Tematica,
				function(success) {
					this.GMS.data.tematicas = success.record;
				}.bind(this),
				function(error) {
				}.bind(this)
			);
		},
		getEstados: function() {
			this.ModelService.EstadoOcorrencia.table_name = this.ModelService.tables.EstadoOcorrencia;
			this.ModelService.EstadoOcorrencia.order = 'nome ASC';
			this.ModelService.Api.getRecordsByFilter(
				this.ModelService.EstadoOcorrencia,
				function(success) {
					this.GMS.data.estados = success.record;
					this.GMS.map.markers.estados = [];
					success.record.forEach(function(estado) {
						this.GMS.map.markers.estados.push(
							{
								idKey: 'estado_'+estado.id,
								coords: {
									latitude: estado.latitude,
									longitude: estado.longitude
								},
								zoom: estado.zoom,
								estado_id: estado.id,
								nome: estado.nome,
								sigla: estado.sigla,
								options: {
									labelContent: estado.nome,
									labelClass:'label label-primary GMapsLabels',
									labelAnchor: '16 -1'
								},
								icon: 'img/markers/estado.png'
							}
						);
					}.bind(this));
				}.bind(this),
				function(error) {
				}.bind(this)
			);
		},
		getMunicipios: function(newv, oldv) {
			if (newv == oldv) return false;
			if (newv == null) {
				this.GMS.data.municipios = [];
				return false;
			}
			this.ModelService.MunicipioOcorrencia.table_name = this.ModelService.tables.MunicipioOcorrencia;
			this.ModelService.MunicipioOcorrencia.order = 'nome ASC';
			this.ModelService.MunicipioOcorrencia.filter = 'estado_id = '+newv;
			this.ModelService.Api.getRecordsByFilter(
				this.ModelService.MunicipioOcorrencia,
				function(success) {
					this.GMS.data.municipios = success.record;
					this.GMS.map.markers.municipios = [];
					success.record.forEach(function(municipio) {
						this.GMS.map.markers.municipios.push(
							{
								idKey: 'municipio_'+municipio.id,
								coords: {
									latitude: municipio.latitude,
									longitude: municipio.longitude
								},
								municipio_id: municipio.id,
								estado_id: municipio.estado_id,
								icon: 'img/markers/logo_ej_marker.png'
							}
						);
					}.bind(this));
				}.bind(this),
				function(error) {
				}.bind(this)
			);
		},
		getOcorrencias: function() {
			this.GMS.gmapLoading = true;
			this.ModelService.Ocorrencia.table_name = this.ModelService.tables.Ocorrencia;
			
			this.ModelService.Ocorrencia.filter = '';
			if (this.GMS.geoActions.getVisao() == 'A') {
				this.ModelService.Ocorrencia.filter = 'municipio_id = '+this.GMS.search.municipio_id;
			}
			if (this.GMS.geoActions.getVisao() == 'M') { 
				this.ModelService.Ocorrencia.filter = 'estado_id = '+this.GMS.search.estado_id;
			}
			
			this.ModelService.Api.getRecordsByFilter(
				this.ModelService.Ocorrencia,
				function(success) {
					this.GMS.data.ocorrencias = success.record;
					this.GMS.map.markers.ocorrencias = [];
					ocorrencias_ids = [];
					success.record.forEach(function(ocorrencia) {
						ocorrencias_ids.push(ocorrencia.id);
					});
					this.ModelService.Api.getRecordsByFilter(
						{
							table_name: 'localizacoes',
							filter: 'ocorrencia_municipal_id in ('+ocorrencias_ids.toString(',')+')'
						},
						function(success2) {
							this.GMS.gmapLoading = false;
							success2.record.forEach(function(localizacao) {
								this.GMS.map.markers.ocorrencias.push(
									{
										idKey: 'localizacao_'+localizacao.id,
										coords: {
											latitude: localizacao.latitude,
											longitude: localizacao.longitude
										},
										localizacao_id: localizacao.id,
										ocorrencia_id: localizacao.ocorrencia_municipal_id,
										icon: 'img/markers/localizacao.png',
									}
								);
							}.bind(this));
						}.bind(this)
					);
				}.bind(this),
				function(error) {
					this.GMS.gmapLoading = false;
				}.bind(this)
			);
		},
	}
});
