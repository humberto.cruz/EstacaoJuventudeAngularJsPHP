estacaoApp.cC({
	name: 'OcorrenciaOrgaoController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		this.Ocorrencia = this.ModelService.model.Ocorrencia;
		this.OcorrenciaOrgao = this.ModelService.model.OcorrenciaOrgao;
		this.$.loading = false;
		this.$.norecords = false;
		this._local();
	},
	watch: {
	},
	methods: {
		_local: function(){
			this.$.header = 'Órgãos Executores da Ocorrência';
			this.$.loading = true;
			this.$.norecords = false;
			this.OcorrenciaOrgao.get(
				{
					q: 'OcorrenciaOrgao.ocorrencia_municipal_id.eq.'+this.$routeParams.id,
					populate: 'Orgao'
				}
			).then(function(data){
				this.$.Orgaos = data.data;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		add: function() {
			this.$location.path('/ocorrencias/'+this.$routeParams.id+'/orgaos/add');
		},
		edit: function(item) {
			this.$location.path('/ocorrencias/'+this.$routeParams.id+'/orgaos/edit/'+item.OcorrenciaOrgao.id);
		},
		del: function(item) {
			if (!confirm('Tem Certeza?')) return false;
			this.OcorrenciaOrgao.del(item.id).then(function(data){
				this._local();
			}.bind(this));
		},
		cancel: function() {
			this.$location.path('/ocorrencias');
		}
	}
});
