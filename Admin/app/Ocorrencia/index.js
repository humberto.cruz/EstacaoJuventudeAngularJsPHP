estacaoApp.cC({
	name: 'OcorrenciaController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		this.$.loading = false;
		this.$.norecords = false;
		// Para facilitar
		this.Ocorrencia = this.ModelService.model.Ocorrencia;
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Ocorrencia.params.page = newv;
				this._load();
			}
		},
		_load: function() {
			this.DataService.resetTimer = true;
			this.$.loading = true;
			// Ler as noticias do banco através do ModelService
			// Todo: Tem uma forma melhor, sem usar o .then ?
			this.Ocorrencia.get(
				{
					page: this.Ocorrencia.params.page,
					sort: '-id',
					populate: 'Programa,Municipio.Estado,Localizacao,OcorrenciaOrgao'
				}
			).then(function(data){
				this.$.Ocorrencias = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		permit: function(item) {
			user = this.DataService.Usuario;

			if (user.nivel == 'Nacional') return true;
			else if (user.nivel == 'Estadual' && user.estado_id == item.Ocorrencia.estado_id) return true;
			else if (user.nivel == 'Municipal' && user.municipio_id == item.Ocorrencia.municipio_id) return true;
			else return false;
		},
		add: function() {
			this.$location.path('ocorrencias/add');
		},
		edit: function(item) {
			this.$location.path('ocorrencias/edit/'+item.Ocorrencia.id);
		},
		localizacoes: function(item) {
			this.$location.path('ocorrencias/localizacoes/'+item.Ocorrencia.id);
		},
		orgaos: function(item) {
			this.$location.path('ocorrencias/'+item.Ocorrencia.id+'/orgaos');
		}
	}
});
