estacaoApp.cC({
	name: 'PerfilController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		this.Usuario = this.ModelService.model.Usuario;
		this.Grupo = this.ModelService.model.Grupo;

		this._edit();
	},
	watch: {
		'{object}Form.estado_id':'_changeEstado'
	},
	methods: {
		_related: function() {
			this.Grupo.get({
				limit: 100
			})
			.then(function(data){
				this.$.Grupos = data.data;
			}.bind(this));
		},
		_edit: function() {
			this.$.header = 'Editar Perfil';
			this._related();
			this.Usuario.get({id: this.DataService.Usuario.id})
			.then(function(data){
				this.$.Form = data.data.Usuario;
			}.bind(this));
		},
		save: function() {
			this.Usuario.save(this.$.Form);
			this.$location.path('/usuarios');
		},
		cancel: function() {
			this.$location.path('/usuarios');
		}
	}
});
