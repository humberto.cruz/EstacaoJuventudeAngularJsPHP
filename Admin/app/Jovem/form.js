estacaoApp.cC({
	name: 'JovemFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		this.Jovem = this.ModelService.model.Jovem;
		this.$.map = {
			center: [-15,-49],
			zoom: 4,
			options: function() {
				return {
				};
			},
			events: {
				click: function(mouse) {

				}.bind(this),
				idle: function() {

				}.bind(this)
			}
		};
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
	},
	methods: {
		_related: function() {
			this.Estado.get()
			.then(function(data){
				this.$.Estados = data.data;
			}.bind(this));
		},
		moveMarker: function(e) {
			latLng = e.latLng;
			this.$.Form.latitude = latLng.k;
			this.$.Form.longitude = latLng.D;
		},
		_add: function(){
			this.$.header = 'Novo Estado';
			this._related();
		},
		load: function() {
			this.$.loading = true;
			this.Estado.get({id: this.$routeParams.id})
			.then(function(data){
				Estado = data.data.Estado;
				Estado.latitude = parseFloat(Estado.latitude);
				Estado.longitude = parseFloat(Estado.longitude);
				this.$.Form = Estado;
				this.$.marker = {
					position: [Estado.latitude, Estado.longitude],
					decimals: 5,
					options: function () {
						return {
							draggable: true
						}
					}
				};
				this.$.loading = false;
			}.bind(this));
		},
		_edit: function() {
			this.$.header = 'Editar Estado';
			this._related();
			this.load();
		},
		save: function() {
			this.$.Form.latitude = this.$.marker.position[0];
			this.$.Form.longitude = this.$.marker.position[1];
			this.Estado.save(this.$.Form);
			this.$location.path('/estados');
		},
		cancel: function() {
			this.$location.path('/estados');
		}
	}
});
