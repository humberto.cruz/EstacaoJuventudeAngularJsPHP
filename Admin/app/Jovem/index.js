estacaoApp.cC({
	name: 'JovemController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		this.$.loading = false;
		this.$.norecords = false;
		// Pra facilitar
		this.Jovem = this.ModelService.model.Jovem;
		// Paginação
		this.$.pagination = {
			page: 1,
			limit: 10,
			count: 0,
			pageCount: 1
		};
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Estado.params.page = newv;
				this._load();
			}
		},
		_load: function() {
			this.$.loading = true;
			// Ler as noticias do banco através do ModelService
			// Todo: Tem uma forma melhor, sem usar o .then ?
			this.Jovem.get(
				{
					page: this.Jovem.params.page,
					sort: 'nome'
				}
			).then(function(data){
				this.$.Jovens = data.data;
				this.$.loading = false;
			}.bind(this));
		},
		add: function() {
			this.$location.path('jovens/add');
		},
		edit: function(item) {
			this.$location.path('jovens/'+item.Estado.id+'/edit');
		}
	}
});
