estacaoApp.cC({
	name: 'ProgramaFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams','growl'],
	data: {
		DataService: 'DataService'
	},
	init: function() {
		
	},
	watch: {
		'{object}DataService.appStatus':'_start',
	},
	methods: {
		_start: function(newv, oldv) {
			if (newv == oldv) return false;
			if (this.$routeParams.id) this._edit();
			else this._add();
		},
		_related: function() {
		},
		_add: function(){
			this.header = 'Novo Programa';
			this.Form = {};
			this._related();
		},
		_edit: function() {
			this.header = 'Editar Programa';
			this.loading = true;
			this.ModelService.actions.getRecord(
				'Programa',
				this.$routeParams.id,
				function(data){
					this.Form = data.record[0];
					this.loading = false;
					this._related();
				}.bind(this),
				function(data){
				}.bind(this)
			);
		},
		save: function() {
			this.$.saving = true;
			this.Programa.save(this.$.Form).then(function(data){
				this.$.saving = false;
				this.growl.success("Programa salvo com sucesso!");
				this.$location.path('/programas');
			}.bind(this));
		}
	}
});
