estacaoApp.cC({
	name: 'MensagemController',
	inject: ['$scope','ModelService','DataService','$location','$timeout'],
	init: function() {
		this.$.loading = false;
		this.$.norecords = false;
		// Pra facilitar
		this.Mensagem = this.ModelService.model.Mensagem;
		this.Pasta = this.ModelService.model.Pasta;
		this.Usuario = this.ModelService.model.Usuario;

		// Método ao iniciar
		this.$timeout(function () {
			this.pasta_id = 1;
			this.$.pasta_nome = 'Caixa de Entrada';
			this._load();
		}.bind(this), 1000);
	},
	
	methods: {
		
		_load: function(newv, oldv) {
			this.$.status = 'messages';
			delete(this.$.Mensagem);

			this.DataService.resetTimer = true;

			this.$.loading = true;
			this.$.norecords = false;
			this.$.Mensagens = [];
			this.Mensagem.get(
				{
					sort: '-data',
					populate: 'Destinatario,Remetente',
					q: 'Mensagem.destinatario_id.eq.'+this.DataService.Usuario.id+',Mensagem.pasta_id.eq.'+this.pasta_id
				}
			).then(function(data){
				this.$.Mensagens = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));

			// Pastas do Sistema
			this.Pasta.get({
				q: 'Pasta.usuario_id.nu',
				order: 'id'
			}).then(function(data){
				this.$.PastasSistema = data.data;
				this.$.pagination = data.pagination;
				//this.$.loading = false;
				
			}.bind(this));
			// Patas do Usuário
			this.Pasta.get({
				q: 'Pasta.usuario_id.eq.'+this.DataService.Usuario.id
			}).then(function(data){
				this.$.PastasUsuario = data.data;
				this.$.pagination = data.pagination;
				//this.$.loading = false;
				
			}.bind(this));
		},
		messages: function(item) {
			if (item == undefined) {
				this.$.status = 'messages';
			} else if (this.pasta_id != item.Pasta.id) {
				this.pasta_id = item.Pasta.id;
				this.$.pasta_nome = item.Pasta.nome;
				this._load();
			} else {
				this.$.status = 'messages';
			}
		},
		newMessage: function(tipo) {
			if (tipo == 'responder') {
				this.$.Form = {
					pasta_id: 1,
					remetente_id: this.DataService.Usuario.id,
					destinatario_id: this.$.Mensagem.Mensagem.remetente_id,
					busca_destinatario: this.$.Mensagem.Remetente.nome,
					assunto: 'RES: '+this.$.Mensagem.Mensagem.assunto,
					body: '<hr>'+this.$.Mensagem.Mensagem.body
				};
				this.searchDestinatario();
			} else if (tipo == 'encaminhar') {
				this.$.Form = {
					pasta_id: 1,
					remetente_id: this.DataService.Usuario.id,
					assunto: 'ENC: '+this.$.Mensagem.Mensagem.assunto,
					body: '<hr>'+this.$.Mensagem.Mensagem.body
				};
			} else {
				this.$.Form = {
					pasta_id: 1,
					remetente_id: this.DataService.Usuario.id
				};
			this.$.Form.busca_destinatario = '';
			}
			this.$.status = 'newMessage';
		},
		searchDestinatario: function() {
			if (this.$.Form.busca_destinatario.length < 3) return false;
			this.Usuario.get(
				{
					q: 'Usuario.nome.lk.'+this.$.Form.busca_destinatario
				}
			).then(function(data){
				this.$.destinatarios = data.data;
			}.bind(this));
		},
		saveMenssage: function() {
			this.$.Form.dono_id = this.$.Form.destinatario_id
			this.Mensagem.save(this.$.Form).then(function(data){
				this.messages();
			}.bind(this));
		},
		viewMessage: function(item) {
			this.$.Mensagem = item;
			this.$.status = 'viewMessage';
		},
		newMarcador: function() {
			console.log('novo marcador');
		},
		marcar: function(item) {
			console.log('marcar msg');
		}
	}
});
