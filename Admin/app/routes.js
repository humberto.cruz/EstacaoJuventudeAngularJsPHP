/*
 Arquivo com as rotas em uso pelo ngRoute do AngularJS

*/

estacaoApp.config(function($routeProvider) {
	$routeProvider
	.when('/', {
		controllerAs: 'home',
		controller: 'homeCtrl',
		templateUrl: '/app/Home/index.html'
	})

	// Programas
	.when('/programas', {
		controller: 'ProgramaController',
		templateUrl: '/app/Programa/index.html',
		controllerAs: 'ProgramaCtrl'
	})
	.when('/programas/add', {
		controller: 'ProgramaFormController',
		templateUrl: '/app/Programa/form.html',
		controllerAs: 'ProgramaFormCtrl'
	})
	.when('/programas/:id/edit', {
		controller: 'ProgramaFormController',
		templateUrl: '/app/Programa/form.html',
		controllerAs: 'ProgramaFormCtrl'
	})
	.when('/programas/:id/tematicas', {
		controller: 'ProgramaTematicasController',
		templateUrl: '/app/Programa/tematicas.html',
		controllerAs: 'ProgramaTematicaCtrl'
	})
	.when('/programas/:id/orgaos', {
		controller: 'ProgramaOrgaosController',
		templateUrl: '/app/Programa/orgaos.html',
		controllerAs: 'ProgramaOrgaoCtrl'
	})
	.when('/programas/:id/aprovacoes', {
		controller: 'ProgramaAprovacoesController',
		templateUrl: '/app/Programa/aprovacoes.html',
		controllerAs: 'ProgramaAprovacoesCtrl'
	})
	
	// Tematicas
	.when('/tematicas', {
		controller: 'TematicaController',
		templateUrl: '/app/Tematica/index.html',
		controllerAs: 'TematicaCtrl'
	})
	.when('/tematicas/add', {
		controller: 'TematicaFormController',
		templateUrl: '/app/Tematica/form.html',
		controllerAs: 'TematicaFormCtrl'
	})
	.when('/tematicas/:id', {
		controller: 'TematicaFormController',
		templateUrl: '/app/Tematica/form.html',
		controllerAs: 'TematicaFormCtrl'
	})

	// Jovens
	.when('/jovens', {
		controller: 'JovemController',
		templateUrl: '/app/Jovem/index.html'
	})
	.when('/estados/add', {
		controller: 'JovemFormController',
		templateUrl: '/app/Jovem/form.html'
	})
	.when('/estados/:id/edit', {
		controller: 'JovemFormController',
		templateUrl: '/app/Jovem/form.html'
	})
	// Estados
	.when('/estados', {
		controller: 'EstadoController',
		templateUrl: '/app/Estado/index.html'
	})
	.when('/estados/add', {
		controller: 'EstadoFormController',
		templateUrl: '/app/Estado/form.html'
	})
	.when('/estados/:id/edit', {
		controller: 'EstadoFormController',
		templateUrl: '/app/Estado/form.html'
	})
	// Mensagens
	.when('/mensagens', {
		controller: 'MensagemController',
		templateUrl: '/app/Mensagem/index.html'
	})

	

	// Orgãos Executores
	.when('/orgaos', {
		controller: 'OrgaoController',
		templateUrl: '/app/Orgao/index.html'
	})
	.when('/orgaos/add', {
		controller: 'OrgaoFormController',
		templateUrl: '/app/Orgao/form.html'
	})
	.when('/orgaos/:id/edit', {
		controller: 'OrgaoFormController',
		templateUrl: '/app/Orgao/form.html'
	})

	// Ocorrencias
	.when('/ocorrencias', {
		controller: 'OcorrenciaController',
		templateUrl: '/app/Ocorrencia/index.html'
	})
	.when('/ocorrencias/add', {
		controller: 'OcorrenciaFormController',
		templateUrl: '/app/Ocorrencia/form.html'
	})
	.when('/ocorrencias/edit/:id', {
		controller: 'OcorrenciaFormController',
		templateUrl: '/app/Ocorrencia/form.html'
	})
	.when('/ocorrencias/localizacoes/:id', {
		controller: 'OcorrenciaLocalController',
		templateUrl: '/app/Ocorrencia/localizacoes.html'
	})
	.when('/ocorrencias/localizacoes/:id/add', {
		controller: 'OcorrenciaLocalFormController',
		templateUrl: '/app/Ocorrencia/localizacoesForm.html'
	})
	.when('/ocorrencias/localizacoes/:id/edit/:localizacao_id', {
		controller: 'OcorrenciaLocalFormController',
		templateUrl: '/app/Ocorrencia/localizacoesForm.html'
	})
	.when('/ocorrencias/:id/orgaos', {
		controller: 'OcorrenciaOrgaoController',
		templateUrl: '/app/Ocorrencia/orgaos.html'
	})
	.when('/ocorrencias/:id/orgaos/add', {
		controller: 'OcorrenciaOrgaoFormController',
		templateUrl: '/app/Ocorrencia/orgaosForm.html'
	})
	.when('/ocorrencias/:id/orgaos/edit/:orgao_id', {
		controller: 'OcorrenciaOrgaoFormController',
		templateUrl: '/app/Ocorrencia/orgaosForm.html'
	})
	// Usuários
	.when('/usuarios', {
		controller: 'UsuarioController',
		controllerAs: 'UsuarioCtrl',
		templateUrl: '/app/Usuario/index.html'
	})
	.when('/usuarios/add', {
		controller: 'UsuarioFormController',
		controllerAs: 'UsuarioFormCtrl',
		templateUrl: '/app/Usuario/form.html'
	})
	.when('/usuarios/edit/:id', {
		controller: 'UsuarioFormController',
		controllerAs: 'UsuarioFormCtrl',
		templateUrl: '/app/Usuario/form.html'
	})
    // Municipios
	.when('/municipios', {
		controller: 'MunicipioController',
		templateUrl: '/app/Municipio/index.html'
	})
    .when('/municipios/add', {
		controller: 'MunicipioFormController',
		templateUrl: '/app/Municipio/form.html'
	})
	.when('/municipios/edit/:id', {
		controller: 'MunicipioFormController',
		templateUrl: '/app/Municipio/form.html'
	})
	// Perfil do Usuario
	.when('/perfil', {
	controller: 'PerfilController',
	templateUrl: '/app/Perfil/index.html'
	})

	// Se não for nenhuma das rotas acima, mostrar mensagem de erro 404 ( não encontrado )
	.otherwise({
		templateUrl: '/app/errors/404.html'
	});

});
