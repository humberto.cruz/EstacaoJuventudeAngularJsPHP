/*
* Iniciando Aplicação
*
*/
// Definicão do Módulo Angular
var estacaoApp = angular.module('estacaoApp',
	[
	'angular-growl',
	'classy',
	'ngResource',
	'ngRoute',
	'ngSanitize',
	'ui.bootstrap',
	'ngWig',
	'ngDreamFactory',
	'ngCookies',
	'ngDialog',
	]
);

// Adicionando constantes para uso do DreamFactory
estacaoApp
	.constant('DSP_URL', 'http://estacao2.jsapps.com.br')
	.constant('DSP_API_KEY', 'estacaoadmin');

// Configurando angular-classy para uso de ControllerAs ( sem $scope )
// estacaoApp.classy.options.controller = {
// addToScope: false
// };

// Adicionando header para comunicação com a API DreamFactory
estacaoApp
	.config(['$httpProvider', 'DSP_API_KEY', function($httpProvider, DSP_API_KEY) {
		$httpProvider.defaults.headers.common['X-DreamFactory-Application-Name'] = DSP_API_KEY;
	}]);

// Configurando tempo geral para mostrar as mensagens do Growl
estacaoApp.config(['growlProvider', function(growlProvider) {
    growlProvider.globalTimeToLive(5000);
}]);
