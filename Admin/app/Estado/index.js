estacaoApp.cC({
	name: 'EstadoController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		this.$.loading = false;
		this.$.norecords = false;
		// Pra facilitar
		this.Estado = this.ModelService.model.Estado;
		// Paginação
		this.$.pagination = {
			page: 1,
			limit: 10,
			count: 0,
			pageCount: 1
		};
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Estado.params.page = newv;
				this._load();
			}
		},
		_load: function() {
			this.$.loading = true;
			// Ler as noticias do banco através do ModelService
			// Todo: Tem uma forma melhor, sem usar o .then ?
			this.Estado.get(
				{
					page: this.Estado.params.page,
					sort: 'sigla',
					populate: 'Municipio'
				}
			).then(function(data){
				this.$.Estados = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		add: function() {
			this.$location.path('estados/add');
		},
		edit: function(item) {
			this.$location.path('estados/'+item.Estado.id+'/edit');
		}
	}
});
